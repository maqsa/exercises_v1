package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        Human gr1 = new Human("Дед1", true, 70, null, null);
        Human gr2 = new Human("Дед2", true, 70, null, null);
        Human grm1 = new Human("Баба1", false, 70, null, null);
        Human grm2 = new Human("Баба2", false, 70, null, null);

        Human father = new Human("Батя", true, 30, gr1, grm1);
        Human mother = new Human("Матя", false, 30, gr2, grm2);

        Human ch1 = new Human("Дитё1", true, 5, father, mother);
        Human ch2 = new Human("Дитё2", false, 7, father, mother);
        Human ch3 = new Human("Дитё3", true, 10, father, mother);

        System.out.println(gr1);
        System.out.println(gr2);
        System.out.println(grm1);
        System.out.println(grm2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(ch1);
        System.out.println(ch2);
        System.out.println(ch3);

    }

    public static class Human
    {
        private String name;
        private boolean sex;
        private int age;
        Human father;
        Human mother;
        //напишите тут ваш код

        public Human(String name, boolean sex, int age, Human father, Human mother){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }
        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
