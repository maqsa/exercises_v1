package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException, InterruptedException
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;
        while (true){
            s = reader.readLine();
                if (s.equals("exit")) break;
                else {
                    Thread thread = new ReadThread(s);
                    thread.start();
                    thread.join();
                }
        }
        reader.close();
    }

    public static class ReadThread extends Thread {
        public String fileName;
        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run()
        {
            try
            {
                FileInputStream inputStream = new FileInputStream(fileName);
                HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
                int count = 0;
                while (inputStream.available() > 0){
                    int data = inputStream.read();
                    if (map.keySet().contains(data)){
                        count = map.get(data) + 1;
                        map.put(data, count);
                    }
                    else map.put(data, 1);
                }
                maxResult(map);
                inputStream.close();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        private synchronized void maxResult(HashMap<Integer, Integer> map)
        {
            int max = new TreeSet<Integer>(map.values()).last();
            for (Integer c : map.keySet()){
                if (map.get(c).equals(max))
                    resultMap.put(this.fileName, c);
            }
        }
    }
}
