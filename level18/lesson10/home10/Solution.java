package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader readerCons = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> fileNameArr = new ArrayList<>();

        String fileName;
        while (true) { // заполнение массива именами файлов
            fileName = readerCons.readLine();
            if (fileName.equals("end")) {       // проверка на "end"
                break;
            } else {
                fileNameArr.add(fileName);
            }
        }
        readerCons.close();

        Collections.sort(fileNameArr); // сортируем имена файлов по порядку

        String fileOutput = fileNameArr.get(0).substring(0, fileNameArr.get(0).indexOf(".part"));
        FileOutputStream summFile = new FileOutputStream(fileOutput);

        for (int i = 0; i < fileNameArr.size(); i++) {
            FileInputStream input = new FileInputStream(fileNameArr.get(i));
            byte[] buff = new byte[input.available()];
            while (input.available() > 0) {
                input.read(buff);
            }
            summFile.write(buff);
            input.close();
        }
        summFile.close();
    }
}