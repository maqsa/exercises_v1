package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sFile = reader.readLine();
        BufferedReader fileOne = new BufferedReader(new InputStreamReader(new FileInputStream(sFile)));
        if(args[0].equals("-u"))
        {

            String addItemLine = productName(args[2]) + price(args[3]) + quantity(args[4]);
            ArrayList<String> arrayList = idUpdate(sFile, fileOne, args[1], addItemLine);

            try
            {
                for (String s : arrayList)
                    printlnAppen(sFile, s);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

        }
        else if (args[0].equals("-d")){
            ArrayList<String> arrayList = idDelete(sFile, fileOne, args[1]);

            try
            {
                for (String s : arrayList)
                    printlnAppen(sFile, s);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        else System.out.println("FATAL_ERROR!");
    }

    private static ArrayList<String> idDelete(String sFile, BufferedReader fileOne, String arg)
    {
        ArrayList<String> allIds = new ArrayList<>();
        String line;
        Long currentID = Long.parseLong(arg);
        try
        {
            while((line=fileOne.readLine())!=null) {
                allIds.add(line);

            }
            fileOne.close();

            for (int i = 0; i < allIds.size(); i++){
                if ((line=allIds.get(i))!=null){
                    line = line.substring(0, 8).replaceAll("\\s", "");
                    if (Long.parseLong(line) == currentID){
                        allIds.remove(i);
                    }
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try {
            FileWriter fstream1 = new FileWriter(sFile);// конструктор с одним параметром - для перезаписи
            BufferedWriter out1 = new BufferedWriter(fstream1); //  создаём буферезированный поток
            out1.write(""); // очищаем, перезаписав поверх пустую строку
            out1.close(); // закрываем
        } catch (Exception e)
        {
            System.err.println("Error in file cleaning: " + e.getMessage());
        }


        return allIds;
    }

    private static void printlnAppen(String filename, String text) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new FileOutputStream(filename, true), true);
        printStream.println(text);
        printStream.close();
    }

    private static ArrayList<String> idUpdate(String sFile, BufferedReader fileOne, String arg, String addItemLine)
    {

        ArrayList<String> allIds = new ArrayList<>();
        String line;
        Long currentID = Long.parseLong(arg);
        try
        {
            while((line=fileOne.readLine())!=null) {
                allIds.add(line);

            }
            fileOne.close();

            for (int i = 0; i < allIds.size(); i++){
                if ((line=allIds.get(i))!=null){
                    line = line.substring(0, 8).replaceAll("\\s", "");
                    if (Long.parseLong(line) == currentID){
                        String tmp = currentID.toString();
                        while (tmp.length() < 8) tmp = tmp + " ";
                        allIds.set(i, tmp+addItemLine);
                    }
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try {
            FileWriter fstream1 = new FileWriter(sFile);// конструктор с одним параметром - для перезаписи
            BufferedWriter out1 = new BufferedWriter(fstream1); //  создаём буферезированный поток
            out1.write(""); // очищаем, перезаписав поверх пустую строку
            out1.close(); // закрываем
        } catch (Exception e)
        {
            System.err.println("Error in file cleaning: " + e.getMessage());
        }


        return allIds;
    }

    private static String productName(String s){
        while (s.length() < 30){
            s = s + " ";
        }
        return s;
    }

    private static String price(String s){
        while (s.length() < 8){
            s = s + " ";
        }
        return s;
    }

    private static String quantity(String s){
        while (s.length() < 4){
            s = s + " ";
        }
        return s;
    }
}
