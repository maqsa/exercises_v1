package com.javarush.test.level18.lesson10.bonus02;

/* Прайсы
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается со следующим набором параметров:
-c productName price quantity
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-c  - добавляет товар с заданными параметрами в конец файла, генерирует id самостоятельно, инкрементируя максимальный id, найденный в файле

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Solution {

    public static void main(String[] args) throws Exception {

        if(args[0].equals("-c"))
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String sFile = reader.readLine();
            BufferedReader fileOne = new BufferedReader(new InputStreamReader(new FileInputStream(sFile)));


            String addItemLine = id(fileOne) + productName(args[1]) + price(args[2]) + quantity(args[3]);
            try
            {
                printlnAppen(sFile, addItemLine);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

        } else System.out.println("FATAL_ERROR!");
    }

    private static void printlnAppen(String filename, String text) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new FileOutputStream(filename, true), true);
        printStream.println(text);
        printStream.close();
    }

    private static String id(BufferedReader fileOne)
    {

        ArrayList<Long> allIds = new ArrayList<>();
        String line;
        Long currentID;
        try
        {
            while((line=fileOne.readLine())!=null) {
                line = line.substring(0, 8).replaceAll("\\s", "");
                currentID = Long.parseLong(line);
                allIds.add(currentID);
            }
            fileOne.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Long maxID = Collections.max(allIds);
        Long myID = maxID+1;
        String tmp = myID.toString();
        while (tmp.length() < 8) tmp = tmp + " ";
        return tmp;
    }

    private static String productName(String s){
        while (s.length() < 30){
            s = s + " ";
        }
        return s;
    }

    private static String price(String s){
        while (s.length() < 8){
            s = s + " ";
        }
        return s;
    }

    private static String quantity(String s){
        while (s.length() < 4){
            s = s + " ";
        }
        return s;
    }
}
