package com.javarush.test.level18.lesson03.task05;

import java.io.FileInputStream;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;




/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream in = new FileInputStream(new Scanner(System.in).nextLine());

        int n = in.available();
        Set<Integer> s = new TreeSet<>();
        for (int i =0; i < n; i++){
            s.add(in.read());
        }

        for (int i : s)
        {
            System.out.printf("%d ", i);
        }
    }
}
