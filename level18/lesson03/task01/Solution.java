package com.javarush.test.level18.lesson03.task01;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* Максимальный байт
Ввести с консоли имя файла
Найти максимальный байт в файле, вывести его на экран.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        String name = "";
        int tmp = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        name = reader.readLine();

        FileInputStream inputStream = new FileInputStream(name);
        while (inputStream.available() > 0){
            int t = inputStream.read();
            if (t > tmp) tmp = t;
        }
        System.out.println(tmp);
        reader.close();
        inputStream.close();
    }
}