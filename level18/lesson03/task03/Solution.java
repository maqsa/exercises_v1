package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байт или байты с максимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream st = new FileInputStream(new Scanner(System.in).nextLine());

        int n = st.available();
        Map<Integer,Integer> map = new HashMap<>();

        for (int i = 0; i < n; i++)
        {
            int data = st.read();
            map.put(data, map.get(data) == null? 1: map.get(data)+1);
        }
        int max = 0;
        int maxData = 0;
        for (Map.Entry<Integer, Integer> pair :map.entrySet())
        {
            int key = pair.getKey();
            int value = pair.getValue();

            if (value > max){
                max = value;
                maxData = key;
            }
        }
        for (Map.Entry<Integer, Integer> pair :map.entrySet())
        {
            int key = pair.getKey();
            int value = pair.getValue();

            if (value == max){
                System.out.print(key +" ");
            }
        }

    }
}