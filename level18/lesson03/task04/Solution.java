package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();

        FileInputStream inputStream = new FileInputStream(name);

        Map<Integer, Integer> map = new HashMap<>();
        while (inputStream.available() > 0){
            int data  = inputStream.read();
            map.put(data, map.get(data) == null? 1: map.get(data)+1);
        }

        int min = 10000;
        for (Map.Entry<Integer, Integer> pair : map.entrySet()){
            int key = pair.getKey();
            int value = pair.getValue();
            
            if (value < min) min = value;
        }

        for (Map.Entry<Integer, Integer> pair :map.entrySet())
        {
            int key = pair.getKey();
            int value = pair.getValue();

            if (value == min){
                System.out.print(key +" ");
            }
        }
    }
}
