package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();
        FileInputStream inputStream = new FileInputStream(filename1);
        FileOutputStream outputStream1 = new FileOutputStream(filename2);
        FileOutputStream outputStream2 = new FileOutputStream(filename3);
        int size = inputStream.available();
        int size1, size2;
        if(size%2==1)
        {
            size1 = size / 2+1;
            size2=size/2;
        }
        else
            size1=size2=size/2;
        byte[] buffer1 = new byte[size1];
        int count1 = inputStream.read(buffer1);
        outputStream1.write(buffer1, 0, count1);
        byte[] buffer2 = new byte[size2];
        int count2 = inputStream.read(buffer2);
        outputStream2.write(buffer2, 0, count2);
        reader.close();
        inputStream.close();
        outputStream1.close();
        outputStream2.close();


    }
}