package com.javarush.test.level05.lesson07.task01;

/* Создать класс Friend
Создать класс Friend (друг) с тремя инициализаторами (тремя методами initialize):
- Имя
- Имя, возраст
- Имя, возраст, пол
*/

public class Friend
{
    //напишите тут ваш код
    private String name = null;
    private int y;
    private String sex = null;
    public void initialize(String name)
    {
        this.name = name;
    }

    public void initialize(int y, String name)
    {
        this.name = name;
        this.y = y;
    }
    public void initialize(int y, String name, String sex)
    {
        this.name = name;
        this.y = y;
        this.sex = sex;
    }
}
