package com.javarush.test.level05.lesson09.task01;

/* Создать класс Friend
Создать класс Friend (друг) с тремя конструкторами:
- Имя
- Имя, возраст
- Имя, возраст, пол
*/

public class Friend
{
    //напишите тут ваш код
    private String name = null;
    private int y;
    private String sex = null;
    public Friend(String name)
    {
        this.name = name;
    }

    public Friend(int y, String name)
    {
        this.name = name;
        this.y = y;
    }
    public Friend(int y, String name, String sex)
    {
        this.name = name;
        this.y = y;
        this.sex = sex;
    }
}