package com.javarush.test.level24.lesson02.home01;

/**
 * Created by maqsa on 16.10.2015.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{
    public SelfInterfaceMarkerImpl()
    {
    }
    public void m1(){}
    public void m2(){}
}
