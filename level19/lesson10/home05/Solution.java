package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит слова, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки. Не использовать try-with-resources
*/
import java.io.*;
public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(args[0]));
        BufferedWriter out = new BufferedWriter(new FileWriter(args[1]));

        while (in.ready()){
            String[] readLine = in.readLine().split(" ");
            for (String s : readLine){
                if (s.matches(".*\\d+.*"))
                    out.write(s + " ");
            }
            out.write("\n");
        }
        in.close();
        out.close();
    }
}