package com.javarush.test.level19.lesson10.bonus01;

import java.util.ArrayList;
import java.util.List;

/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
[Файл 1]
строка1
строка2
строка3

[Файл 2]
строка1
строка3
строка4

[Результат - список lines]
SAME строка1
REMOVED строка2
SAME строка3
ADDED строка4
*/
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        String  f1 = console.readLine(),
                f2 = console.readLine();

        BufferedReader file1 = new BufferedReader(new FileReader(f1));
        BufferedReader file2 = new BufferedReader(new FileReader(f2));
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        while (file1.ready()) list1.add(file1.readLine());
        file1.close();
        while (file2.ready()) list2.add(file2.readLine());
        file2.close();

        int countList1 = 0, countList2 = 0;
        while (countList1 < list1.size() && countList2 < list2.size()){
            String line1 = list1.get(countList1);
            String line2 = list2.get(countList2);

            if (line1.equals(line2)){
                lines.add(new LineItem(Type.SAME, line1));
                countList1++;
                countList2++;
            }
            else{
                if (list2.subList(countList2,list2.size()).contains(line1)){
                    //current line1 was found in list2 later, so lines were added into file2
                    lines.add(new LineItem(Type.ADDED, line2));
                    countList2++;
                }
                else {
                    //current line1 was not found in list2 later, so the line was removed
                    lines.add(new LineItem(Type.REMOVED, line1));
                    countList1++;
                }
            }
        }

        //check for leftovers; only one of this loops will be run
        while (countList1 < list1.size()){
            //only file1's lines left - all was removed
            lines.add(new LineItem(Type.REMOVED, list1.get(countList1)));
            countList1++;
        }
        while (countList2 < list2.size()){
            //only file2's lines left -all was added
            lines.add(new LineItem(Type.ADDED, list2.get(countList2)));
            countList2++;
        }

        for (LineItem lineItem : lines)
            System.out.println(lineItem.type + " " + lineItem.line);
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}