package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки. Не использовать try-with-resources

Пример выходных данных:
длинное,короткое,аббревиатура
*/
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(args[0]));
        BufferedWriter out = new BufferedWriter(new FileWriter(args[1]));
        List<String> result = new ArrayList<>();

        while (in.ready()){
            String[] lineAsArray = in.readLine().split(" ");
            for (String s : lineAsArray){
                if (s.length() > 6) result.add(s);
            }
        }
        in.close();

        for(int i = 0; i < result.size(); i++){
            if (i != result.size()- 1)
                out.write(result.get(i) + ",");
            else out.write(result.get(i));
        }
        out.close();
    }
}
