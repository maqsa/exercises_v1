package com.javarush.test.level26.lesson10.home02;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by maqsa on 28.10.2015.
 */
public class Producer implements Runnable
{
    protected ConcurrentHashMap<String, String> map;

    public Producer(ConcurrentHashMap<String, String> map)
    {
        this.map = map;
    }

    @Override
    public void run()
    {
        int i = 1;
        try
        {
            while (true){
                map.put(i+"", String.format("Some text for %d", i++));
                Thread.sleep(500);
            }
        } catch (InterruptedException e){
            System.out.printf("[%s] thread was terminated", Thread.currentThread().getName());
        }
    }
}
