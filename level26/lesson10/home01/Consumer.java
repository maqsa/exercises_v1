package com.javarush.test.level26.lesson10.home01;

import java.util.concurrent.BlockingQueue;

/**
 * Created by maqsa on 28.10.2015.
 */
public class Consumer implements Runnable
{
    protected BlockingQueue queue;

    public Consumer(BlockingQueue queue) {
        this.queue = queue;
    }
    @Override
    public void run()
    {
        try {
            while (true) {
                Thread.sleep(500);
                System.out.println(queue.remove());
            }
        } catch (InterruptedException e) {
            System.out.println(queue.remove());
            System.out.println(String.format("[%s] thread was terminated", Thread.currentThread().getName()));
        }
    }
}