package com.javarush.test.level28.lesson06.home01;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by maqsa on 01.11.2015.
 */
public class MyThread extends Thread
{
    public MyThread()
    {
    }
    private static AtomicInteger counter = new AtomicInteger(0);
    private int max;

    public MyThread(Runnable target){
        super(target);
        this.setPriority(getPrior());

    }
    public MyThread(ThreadGroup group, Runnable target) {
        super(group, target);
        this.setPriority(getPrior());
    }

    public MyThread(String name) {
        super(name);
        this.setPriority(getPrior());
    }

    public MyThread(ThreadGroup group, String name) {
        super(group, name);
        this.setPriority(getPrior());
    }

    public MyThread(Runnable target, String name) {
        super(target, name);
        this.setPriority(getPrior());
    }

    public MyThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
        this.setPriority(getPrior());
    }

    public MyThread(ThreadGroup group, Runnable target, String name, long stackSize) {
        super(group, target, name, stackSize);
        this.setPriority(getPrior());
    }
    public int getPrior(){
        counter.incrementAndGet();
        if(counter.get() <= 10 && counter.get() <= this.getThreadGroup().getMaxPriority()){
            return counter.get();
        }else if (counter.get() <= 10 && counter.get() >= this.getThreadGroup().getMaxPriority())
            return this.getThreadGroup().getMaxPriority();
        else if(counter.get() > 10) {
            counter.set(1);
        }
        return counter.get();
    }
}
