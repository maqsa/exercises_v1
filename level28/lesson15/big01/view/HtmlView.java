package com.javarush.test.level28.lesson15.big01.view;

import com.javarush.test.level28.lesson15.big01.Controller;
import com.javarush.test.level28.lesson15.big01.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.List;


public class HtmlView implements View {
    private final String filePath = "./src/" + getClass().getPackage().getName().replaceAll("\\.", "/") + "/vacancies.html";
    private Controller controller;

    @Override
    public void update(List<Vacancy> vacancies) {
        updateFile(getUpdatedFileContent(vacancies));
    }

    protected Document getDocument() throws IOException {
        return Jsoup.parse(new File(filePath), "UTF-8");
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies) {
        try {
            Document document = getDocument();

            Elements vacancyElements = document.getElementsByAttributeValue("class", "vacancy");
            for (Element element : vacancyElements)
                element.remove();

            Element template = document
                    .getElementsByClass("template")
                    .first();

            Element patern = template.clone();
            patern.removeAttr("style");
            patern.attr("class", "vacancy");

            for (Vacancy vacancy : vacancies) {
                Element clone = patern.clone();
                clone.getElementsByAttributeValue("class", "city").first().text(vacancy.getCity());
                clone.getElementsByAttributeValue("class", "companyName").first().text(vacancy.getCompanyName());
                clone.getElementsByAttributeValue("class", "salary").first().text(vacancy.getSalary());
                clone.getElementsByTag("a").first().text(vacancy.getTitle());
                clone.getElementsByAttribute("href").attr("href", vacancy.getUrl());
                template.before(clone);
            }
            return document.outerHtml();
        } catch (Exception e) {
            e.printStackTrace();
            return "Some exception occurred";
        }
    }

    private void updateFile(String content) {
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"))) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void userCitySelectEmulationMethod() {
        try
        {
            controller.onCitySelect("Odessa");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}