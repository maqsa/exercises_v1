package com.javarush.test.level25.lesson05.home01;

/**
 * Created by maqsa on 25.10.2015.
 */
public class LoggingStateThread extends Thread
{
    Thread target;
    public LoggingStateThread(Thread target)
    {
        this.target = target;
        setDaemon(true);
    }

    public void run(){
        State curState, lastSt = null;
        do{
            curState = target.getState();
            if(lastSt != curState){
                System.out.println(curState);
                lastSt = curState;
            }
        } while (curState != State.TERMINATED);
    }
}
