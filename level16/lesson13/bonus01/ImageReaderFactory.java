package com.javarush.test.level16.lesson13.bonus01;
import com.javarush.test.level16.lesson13.bonus01.common.*;
/**
 * Created by maqsa on 17.09.2015.
 */
public class ImageReaderFactory
{
    static ImageReader getReader( ImageTypes imgType )
    {
        ImageReader imgReader;

        if ( imgType == ImageTypes.BMP )
        {
            imgReader = new BmpReader();
        }
        else if ( imgType == ImageTypes.JPG )
        {
            imgReader = new JpgReader();
        }
        else if ( imgType == ImageTypes.PNG )
        {
            imgReader = new PngReader();
        }
        else
        {
            throw new IllegalArgumentException( "Неизвестный тип картинки" );
        }

        return imgReader;
    }
}
