package com.javarush.test.level29.lesson15.big01.human;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students = new ArrayList<>();
    private String name;
    private int age;

    public University(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student getStudentWithAverageGrade(double averageGrade)
    {
        if (students != null && students.size() != 0)
        {
            for (Student student : students)
            {
                if (student.getAverageGrade() == averageGrade)
                    return student;
            }
        }
        return null;
    }

    public Student getStudentWithMaxAverageGrade() {
        if (students != null && students.size() != 0)
        {
            Student result = students.get(0);
            double max = students.get(0).getAverageGrade();
            for (Student student : students)
            {
                if (student.getAverageGrade() > max)
                {
                    result = student;
                    max = student.getAverageGrade();
                }
            }
            return result;
        }
        return null;
    }

    public void expel(Student student)
    {
        if (students != null && students.contains(student))
            students.remove(student);
    }

    public Student getStudentWithMinAverageGrade()
    {
        if (students != null && students.size() != 0)
        {
            Student result = students.get(0);
            double min = students.get(0).getAverageGrade();
            for (Student student : students)
            {
                if (student.getAverageGrade() < min)
                {
                    result = student;
                    min = student.getAverageGrade();
                }
            }
            return result;
        }
        return null;
    }

    public List<Student> getStudents()
    {
        return students;
    }

    public void setStudents(List<Student> students)
    {
        this.students = students;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
