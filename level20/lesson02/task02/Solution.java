package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
//            File your_file_name = File.createTempFile("your_file_name", null);
            OutputStream outputStream = new FileOutputStream("1.txt");
            InputStream inputStream = new FileInputStream("1.txt");

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            javaRush.users.add(new User());
            javaRush.users.get(0).setFirstName("Ivan");
            javaRush.users.get(0).setLastName("Ivanov");
            javaRush.users.get(0).setBirthDate(new Date(1950, 01, 01));
            javaRush.users.get(0).setMale(true);
            javaRush.users.get(0).setCountry(User.Country.RUSSIA);
            javaRush.users.add(new User());
            javaRush.users.get(1).setFirstName("Ivan");
            javaRush.users.get(1).setLastName("Ivanov");
            javaRush.users.get(1).setBirthDate(new Date(1950, 01, 01));
            javaRush.users.get(1).setMale(true);
            javaRush.users.get(1).setCountry(User.Country.RUSSIA);
            javaRush.users.add(new User());
            User user=null;
            javaRush.users.add(user);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны
            System.out.println(loadedObject.equals(javaRush));
            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<User>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter writer = new PrintWriter(outputStream);
            if (users!=null)
            {
                writer.write(users.size()+"\n");
                for (User user : users)
                {
                    if (user!=null) {
                        writer.write(user.getFirstName() == null ? "null\n" : user.getFirstName() + "\n");
                        writer.write(user.getLastName() == null ? "null\n" : user.getLastName() + "\n");
                        writer.write(user.getBirthDate() == null ? "null\n" : user.getBirthDate().getTime() + "\n");
                        writer.write((user.isMale() ? "true" : "false") + "\n");
                        writer.write(user.getCountry() == null ? "null\n" : user.getCountry().getDisplayedName() + "\n");
                    }
                    else
                        writer.write("-");
                }

            }
            writer.close();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            try
            {
                int n = Integer.parseInt(reader.readLine());
                String s;
                for (int i = 0; i < n; i++) {

                    users.add(new User());

                    s = reader.readLine();
                    if (s.equals("-")) continue;
                    if (!s.equals("null")) users.get(i).setFirstName(s);
                    s = reader.readLine();
                    if (!s.equals("null")) users.get(i).setLastName(s);
                    s = reader.readLine();
                    if (!s.equals("null")) users.get(i).setBirthDate(new Date(Long.parseLong(s)));
                    s = reader.readLine();
                    users.get(i).setMale(s.equals("true")?true:false);
                    s = reader.readLine();
                    if (s.equals("Russia")) users.get(i).setCountry(User.Country.RUSSIA);
                    else if (s.equals("Ukraine")) users.get(i).setCountry(User.Country.UKRAINE);
                    else users.get(i).setCountry(User.Country.OTHER);
                }
                reader.close();
            }
            catch (NumberFormatException e)
            {
                return;
            }
        }
    }
}