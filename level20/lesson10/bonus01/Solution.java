package com.javarush.test.level20.lesson10.bonus01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* Алгоритмы-числа
Число S состоит из M чисел, например, S=370 и M(количество цифр)=3
Реализовать логику метода getNumbers, который должен среди натуральных чисел меньше N (long)
находить все числа, удовлетворяющие следующему критерию:
число S равно сумме его цифр, возведенных в M степень
getNumbers должен возвращать все такие числа в порядке возрастания

Пример искомого числа:
370 = 3*3*3 + 7*7*7 + 0*0*0
8208 = 8*8*8*8 + 2*2*2*2 + 0*0*0*0 + 8*8*8*8

На выполнение дается 10 секунд и 50 МБ памяти.
*/
public class Solution
{
    public static int[] getNumbers(int N)
    {
        int[] result;
        List res = new ArrayList<>();

        int oldLength = 0, newLength;
        long[] degree = new long[10];
        degree[0] = 0;
        degree[1] = 1;
        for (int i = 1; i < N; i++)
        {
            char[] chars = String.valueOf(i).toCharArray();
            newLength = chars.length;
            if (newLength != oldLength){
                for (int j = 2; j < degree.length; j++)
                {
                    degree[j] = (long) Math.pow(j,newLength);
                }
                oldLength = newLength;
            }
            long numb=0;
            for (int j = 0; j < chars.length; j++)
            {
                int i1 = chars[j]-48;
                long pow = degree[i1];
                numb+=pow;
                if (numb>i) break;
            }
            if (numb==i) res.add((int) numb);
        }
        result = new int[res.size()];
        for (int i = 0; i < res.size(); i++)
        {
            result[i] = (int) res.get(i);
        }
        return result;
    }

    public static void main(String[] args)
    {
        Long t0 = System.currentTimeMillis();

        int[] numbers = getNumbers(100_000_000);
        System.out.println(Arrays.toString(numbers));
        Long t1 = System.currentTimeMillis();
        System.out.println("time: " + (t1 - t0) / 1000d + " sec");
        System.out.println("memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024) + " mb");

    }
}
