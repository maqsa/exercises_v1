package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
Метод main не участвует в тестировании.
*/
public class Solution implements Serializable {
    public static void main(String[]args) throws IOException, ClassNotFoundException
    {
        ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream("tmp.tmp"));
        Solution solution=new Solution();
        B b=solution.new B("class b");
        oos.writeObject(b);
        System.out.println(b.name);

        oos.close();
        ObjectInputStream ois=new ObjectInputStream(new FileInputStream("tmp.tmp"));
        B b1= (B) ois.readObject();
        System.out.println(b1.name);
        ois.close();
    }
    public static class A {
        protected String name = "A";

        public A(String name) {
            this.name += name;
        }

        public A()
        {
        }
    }


    public  class B extends A implements Serializable {
        public B(String name) {
            super(name);
            this.name += name;
        }
        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException
        {
            ois.defaultReadObject();
            name= (String) ois.readObject();
        }
        private void writeObject(ObjectOutputStream oos) throws IOException
        {
            oos.defaultWriteObject();
            oos.writeObject(name);
        }
    }
}