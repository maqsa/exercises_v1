package com.javarush.test.level23.lesson06.task02;

/* Рефакторинг
Отрефакторите класс Solution: вынесите все константы в public вложенный(nested) класс Constants.
Запретите наследоваться от Constants.
*/
public class Solution {

    public class ServerNotAccessibleException extends Exception {
        public ServerNotAccessibleException() {
            super(Constants.sinafw);
        }

        public ServerNotAccessibleException(Throwable cause) {
            super(Constants.sinafw, cause);
        }
    }

    public class UnauthorizedUserException extends Exception {
        public UnauthorizedUserException() {
            super(Constants.uina);
        }

        public UnauthorizedUserException(Throwable cause) {
            super(Constants.uina, cause);
        }
    }

    public class BannedUserException extends Exception {
        public BannedUserException() {
            super(Constants.usb);
        }

        public BannedUserException(Throwable cause) {
            super(Constants.usb, cause);
        }
    }

    public class RestrictionException extends Exception {
        public RestrictionException() {
            super(Constants.aid);
        }

        public RestrictionException(Throwable cause) {
            super(Constants.aid, cause);
        }
    }

    public final static class Constants{
        private final static String sinafw = "Server is not accessible for now.";
        private final static String uina = "User is not authorized.";
        private final static String usb = "User is banned.";
        private final static String aid = "Access is denied.";

    }
}
