package com.javarush.test.level14.lesson06.home01;

/**
 * Created by maqsa on 11.09.2015.
 */
public class UkrainianHen extends Hen
{
    public int getCountOfEggsPerMonth() {return 400;}
    String getDescription()
    {
        return super.getDescription() + " Моя страна - " + Country.UKRAINE + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
    }
}