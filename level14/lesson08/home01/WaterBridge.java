package com.javarush.test.level14.lesson08.home01;

/**
 * Created by maqsa on 11.09.2015.
 */
public class WaterBridge implements Bridge
{

    @Override
    public int getCarsCount()
    {
        return 220;
    }
}
