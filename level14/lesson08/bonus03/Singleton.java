package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by maqsa on 13.09.2015.
 */
public class Singleton
{
    private final static Singleton element = new Singleton();

    private Singleton() {
    }

    public static Singleton getInstance()
    {
        return element;
    }

}
