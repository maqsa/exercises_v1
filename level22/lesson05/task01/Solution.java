package com.javarush.test.level22.lesson05.task01;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
*/
public class Solution {
    public static String getPartOfString(String string) {
        try {
            int firstSpace = string.indexOf(" ");
            int fourthSpace=firstSpace;

            for (int i = 0; i < 3; i++) {
                fourthSpace = string.indexOf(" ", fourthSpace+1);
                if (fourthSpace<=firstSpace) throw new TooShortStringException();
            }

            fourthSpace = string.indexOf(" ", fourthSpace + 1);
            fourthSpace = fourthSpace == -1 ? string.length() : fourthSpace;
            return string.substring(firstSpace + 1, fourthSpace);
        } catch (Throwable ignore) {
            throw new TooShortStringException();
        }
    }

    public static class TooShortStringException extends RuntimeException
    {
    }
}
