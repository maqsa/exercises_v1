package com.javarush.test.level22.lesson09.task02;

import java.util.Map;

/* Формируем Where
Сформируйте часть запроса WHERE используя StringBuilder.
Если значение null, то параметр не должен попадать в запрос.
Пример:
{"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null}
Результат:
"name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'"
*/
public class Solution {

    public static StringBuilder getCondition(Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder();
        if(params !=null)
            for (Map.Entry<String, String> entry : params.entrySet()){
                if (entry.getKey()==null | entry.getValue()==null) continue;
                String format = String.format("%s = '%s' and ", entry.getKey(), entry.getValue());
                stringBuilder.append(format);
            }
        if (stringBuilder.length() > 0)
            stringBuilder.replace(stringBuilder.length() - 5, stringBuilder.length(), "");
        return stringBuilder;
    }
}
