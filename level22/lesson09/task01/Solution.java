package com.javarush.test.level22.lesson09.task01;

import java.io.*;
import java.util.*;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример, "мор"-"ром", "трос"-"сорт"
*/

public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(console.readLine()));
        console.close();

        ArrayList<String> strings = new ArrayList<>();

        while (reader.ready()) {
            String[] arr = reader.readLine().split(" ");
            Collections.addAll(strings, arr);
        }
        reader.close();

        for (int j = 0, count = 0; j < strings.size() - count; j++) {
            String first = strings.get(j);
            StringBuilder stringBuilder = new StringBuilder(first);
            stringBuilder.reverse();

            for (int i = 0; i < strings.size(); ) {
                String second = strings.get(i);
                if (stringBuilder.toString().equals(second) & j != i) {
                    Pair pair = new Pair();
                    pair.first = first;
                    pair.second = second;
                    result.add(pair);
                    strings.remove(i);
                    count++;
                } else i++;
            }
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                            second == null && first != null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}