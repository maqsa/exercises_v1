package com.javarush.test.level08.lesson08.task04;

import java.util.*;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", new Date("January 1 1980"));
        map.put("Stallone1", new Date("February 1 1981"));
        map.put("Stallone2", new Date("March 1 1982"));
        map.put("Stallone3", new Date("May 1 1983"));
        map.put("Stallone4", new Date("April 1 1984"));
        map.put("Stallone5", new Date("June 1 1985"));
        map.put("Stallone6", new Date("July 1 1986"));
        map.put("Stallone7", new Date("August 1 1987"));
        map.put("Stallone8", new Date("September 1 1988"));
        map.put("Stallone9", new Date("October 1 1989"));

        //напишите тут ваш код
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        Iterator<Map.Entry<String, Date>> temp = map.entrySet().iterator();
        while (temp.hasNext()) {
            Date d = temp.next().getValue();
            if (d.getMonth() > 4 && d.getMonth() < 8)
                temp.remove();
        }
    }
}
