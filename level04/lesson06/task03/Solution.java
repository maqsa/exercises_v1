package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sn1 = reader.readLine();
        String sn2 = reader.readLine();
        String sn3 = reader.readLine();

        int n1 = Integer.parseInt(sn1);
        int n2 = Integer.parseInt(sn2);
        int n3 = Integer.parseInt(sn3);

        int first =0 , second= 0, third= 0;
        if (n1 > n2 && n1 > n3) {
            first = n1;
            if (n2 > n3){
                second = n2;
                third = n3;
            }else{
                second = n3;
                third = n2;
            }
        }else{
            if (n2 > n1 && n2 > n3) {
                first = n2;
                if (n1 > n3){
                    second = n1;
                    third = n3;
                }else{
                    second = n3;
                    third = n1;
                }

            }else{
                if (n3 > n1 && n3 > n2) {
                    first = n3;
                    if (n2 > n1){
                        second = n2;
                        third = n1;
                    }else {
                        second = n1;
                        third = n2;
                    }
                }
            }
        }
        System.out.println(first + " " + second + " " + third);

    }
}
