package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String sn1 = reader.readLine();
        String sn2 = reader.readLine();
        String sn3 = reader.readLine();
        String sn4 = reader.readLine();

        int n1 = Integer.parseInt(sn1);
        int n2 = Integer.parseInt(sn2);
        int n3 = Integer.parseInt(sn3);
        int n4 = Integer.parseInt(sn4);


        max(n1, n2, n3, n4);
    }


    public static void max(int a, int b, int c, int d)
    {
        if ((a > b) && (a > c)  && (a>d))
            System.out.println(a);
        else if ((b>c) && (b>d))
        {
            System.out.println(b);
        }
        else if (c>d)
        {
            System.out.println(c);
        }
        else System.out.println(d);
    }
    /*public static int max1(int n1, int n2)
    {
        if(n1>n2) return n1;
        else return n2;
    }*/
}
