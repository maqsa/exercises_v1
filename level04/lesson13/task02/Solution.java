package com.javarush.test.level04.lesson13.task02;

import java.io.*;

/* Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
8888
8888
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String ii = reader.readLine();
        String jj = reader.readLine();

        int i = Integer.parseInt(ii);
        int j = Integer.parseInt(jj);

        for (int a = 1; a<=i; a++){
            for (int b = 1; b<=j; b++){
                System.out.print(8);
            }
            System.out.println();
        }
    }
}
