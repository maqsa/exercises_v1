package com.javarush.test.level40.lesson10.task02;

/* Работа с Joda Time
Выполни задание, используя библиотеку Joda Time версии 2.9.1
Реализуй метод printDate(String date).
Он должен в качестве параметра принимать дату (в одном из 3х форматов)
и выводить ее в консоль в соответсвии с примером:

1) Для "21.4.2014 15:56:45" вывод должен быть:
День: 21
День недели: 2
День месяца: 21
День года: 111
Неделя месяца: 4
Неделя года: 17
Месяц: 3
Год: 2014
Эра: 1
AM или PM: 1
Часы: 3
Часы дня: 15
Минуты: 56
Секунды: 45

2) Для "21.4.2014":
День: 21
День недели: 2
День месяца: 21
День года: 111
Неделя месяца: 4
Неделя года: 17
Месяц: 3
Год: 2014
Эра: 1

3) Для "17:33:40":
AM или PM: 1
Часы: 5
Часы дня: 17
Минуты: 33
Секунды: 40
*/

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;

public class Solution {
    public static void main(String[] args) throws ParseException
    {
        printDate("21.4.2014 15:56:45");
        System.out.println();
        printDate("21.4.2014");
        System.out.println();
        printDate("17:33:40");
    }

    public static void printDate(String date) throws ParseException
    {
        if (date.length() == 18)
        {
            DateTimeFormatter df = DateTimeFormat.forPattern("dd.M.yyyy HH:mm:ss");
            DateTime cal = df.parseDateTime(date);

            DateTime minYearDate = cal.dayOfYear().withMinimumValue();
            DateTime minMonthDate = cal.dayOfMonth().withMinimumValue();
            int weekDis = (minYearDate.getWeekyear() == cal.getYear()) ? 0 : 1;
            int weekOfYear = cal.getWeekOfWeekyear() + weekDis;
            if (weekOfYear >= 53)
                weekOfYear = 1;
            int weekOfMonth = cal.getWeekOfWeekyear() - minMonthDate.getWeekOfWeekyear() + 1;
            if (minMonthDate.getWeekOfWeekyear() >= cal.getWeekOfWeekyear())
                weekOfMonth = cal.minusDays(7).getWeekOfWeekyear() - minMonthDate.getWeekOfWeekyear() + 2;
            if (cal.getMonthOfYear() == 1)
                weekOfMonth = weekOfYear;

            System.out.format("День: %d\n" +
                            "День недели: %d\n" +
                            "День месяца: %d\n" +
                            "День года: %d\n" +
                            "Неделя месяца: %d\n" +
                            "Неделя года: %d\n" +
                            "Месяц: %d\n" +
                            "Год: %d\n" +
                            "Эра: %d\n" +
                            "AM или PM: %d\n" +
                            "Часы: %d\n" +
                            "Часы дня: %d\n" +
                            "Минуты: %d\n" +
                            "Секунды: %d\n",
                    cal.getDayOfMonth(),
                    (cal.getDayOfWeek() % 7 + 1),
                    cal.getDayOfMonth(),
                    cal.getDayOfYear(),
                    weekOfMonth,
                    weekOfYear,
                    cal.getMonthOfYear() - 1,
                    cal.getYear(),
                    cal.getEra(),
                    cal.getHourOfDay() >= 12 ? 1 : 0,
                    cal.getHourOfDay() % 12,
                    cal.getHourOfDay(),
                    cal.getMinuteOfHour(),
                    cal.getSecondOfMinute());
        }

        if (date.length() == 9)
        {
            DateTimeFormatter df = DateTimeFormat.forPattern("dd.M.yyyy");
            DateTime cal = df.parseDateTime(date);

            DateTime minYearDate = cal.dayOfYear().withMinimumValue();
            DateTime minMonthDate = cal.dayOfMonth().withMinimumValue();
            int weekDis = (minYearDate.getWeekyear() == cal.getYear()) ? 0 : 1;
            int weekOfYear = cal.getWeekOfWeekyear() + weekDis;
            if (weekOfYear >= 53)
                weekOfYear = 1;
            int weekOfMonth = cal.getWeekOfWeekyear() - minMonthDate.getWeekOfWeekyear() + 1;
            if (minMonthDate.getWeekOfWeekyear() >= cal.getWeekOfWeekyear())
                weekOfMonth = cal.minusDays(7).getWeekOfWeekyear() - minMonthDate.getWeekOfWeekyear() + 2;
            if (cal.getMonthOfYear() == 1)
                weekOfMonth = weekOfYear;

            System.out.format("День: %d\n" +
                            "День недели: %d\n" +
                            "День месяца: %d\n" +
                            "День года: %d\n" +
                            "Неделя месяца: %d\n" +
                            "Неделя года: %d\n" +
                            "Месяц: %d\n" +
                            "Год: %d\n" +
                            "Эра: %d\n",
                    cal.getDayOfMonth(),
                    (cal.getDayOfWeek() % 7 + 1),
                    cal.getDayOfMonth(),
                    cal.getDayOfYear(),
                    weekOfMonth,
                    weekOfYear,
                    cal.getMonthOfYear() - 1,
                    cal.getYear(),
                    cal.getEra());
        }

        if (date.length() == 8)
        {
            DateTimeFormatter df = DateTimeFormat.forPattern("HH:mm:ss");
            DateTime cal = df.parseDateTime(date);

            System.out.format(
                            "AM или PM: %d\n" +
                            "Часы: %d\n" +
                            "Часы дня: %d\n" +
                            "Минуты: %d\n" +
                            "Секунды: %d",
                    cal.getHourOfDay() >= 12 ? 1 : 0,
                    cal.getHourOfDay() % 12,
                    cal.getHourOfDay(),
                    cal.getMinuteOfHour(),
                    cal.getSecondOfMinute());
        }
    }
}
