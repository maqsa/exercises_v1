package com.javarush.test.level40.lesson10.task01;

/* Работа с датами
Реализуй метод printDate(String date).
Он должен в качестве параметра принимать дату (в одном из 3х форматов)
и выводить ее в консоль в соответсвии с примером:

1) Для "21.4.2014 15:56:45" вывод должен быть:
День: 21 +
День недели: 2 +
День месяца: 21 +
День года: 111 +
Неделя месяца: 4 +
Неделя года: 17 +
Месяц: 3 +
Год: 2014 +
Эра: 1 +
AM или PM: 1
Часы: 3 +
Часы дня: 15 +
Минуты: 56 +
Секунды: 45 +

2) Для "21.4.2014":
День: 21
День недели: 2
День месяца: 21
День года: 111
Неделя месяца: 4
Неделя года: 17
Месяц: 3
Год: 2014
Эра: 1

3) Для "17:33:40":
AM или PM: 1
Часы: 5
Часы дня: 17
Минуты: 33
Секунды: 40
*/

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Solution {
    public static void main(String[] args) throws ParseException
    {
        printDate("21.4.2014 15:56:45");
        System.out.println();
        printDate("21.4.2014");
        System.out.println();
        printDate("17:33:40");
    }

    public static void printDate(String date) throws ParseException
    {
        if (date.length() == 18)
        {
            DateFormat df1 = new SimpleDateFormat("dd.M.yyyy hh:mm:ss");

            Date d1 = df1.parse(date);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);

            int era = calendar.get(Calendar.ERA);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            int weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH);
            int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            int amPm = calendar.get(Calendar.AM_PM);
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
            int hour = calendar.get(Calendar.HOUR);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            System.out.format(
                    "День: %d%n" +
                            "День недели: %d%n" +
                            "День месяца: %d%n" +
                            "День года: %d%n" +
                            "Неделя месяца: %d%n" +
                            "Неделя года: %d%n" +
                            "Месяц: %d%n" +
                            "Год: %d%n" +
                            "Эра: %d%n" +
                            "AM или PM: %d%n" +
                            "Часы: %d%n" +
                            "Часы дня: %d%n" +
                            "Минуты: %d%n" +
                            "Секунды: %d%n",
                    day, dayOfWeek, dayOfMonth, dayOfYear, weekOfMonth, weekOfYear, month, year, era, amPm, hour, hourOfDay, minute, second
            );
        }

        if (date.length() == 9){
            DateFormat df1 = new SimpleDateFormat("dd.M.yyyy");

            Date d1 = df1.parse(date);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);

            int era = calendar.get(Calendar.ERA);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            int weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH);
            int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            System.out.format(
                    "День: %d%n" +
                            "День недели: %d%n" +
                            "День месяца: %d%n" +
                            "День года: %d%n" +
                            "Неделя месяца: %d%n" +
                            "Неделя года: %d%n" +
                            "Месяц: %d%n" +
                            "Год: %d%n" +
                            "Эра: %d%n",
                    day, dayOfWeek, dayOfMonth, dayOfYear, weekOfMonth, weekOfYear, month, year, era
            );
        }

        if (date.length() == 8)
        {
            DateFormat df1 = new SimpleDateFormat("hh:mm:ss");

            Date d1 = df1.parse(date);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d1);

            int amPm = calendar.get(Calendar.AM_PM);
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
            int hour = calendar.get(Calendar.HOUR);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            System.out.format(
                            "AM или PM: %d%n" +
                            "Часы: %d%n" +
                            "Часы дня: %d%n" +
                            "Минуты: %d%n" +
                            "Секунды: %d%n",
                    amPm, hour, hourOfDay, minute, second
            );
        }
    }
}
