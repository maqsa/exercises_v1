package com.javarush.test.level40.lesson10.task03;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* В какой день недели день рождения?
Реализуй метод weekDayOfBirthday.
Метод должен возвращать день недели на итальянском языке, в который будет день рождения в определенном году.
Пример формата дат смотри в методе main.

Пример:
1) Для "30.05.1984" и "2015" метод должен вернуть: sabato
2) Для "1.12.2015" и "2016" метод должен вернуть: giovedì

Выполни задание, используя библиотеку Joda Time версии 2.9.1
*/

public class Solution {
    public static void main(String[] args) throws ParseException
    {
        System.out.println(weekDayOfBirthday("13.02.1992", "2016"));
    }

    public static String weekDayOfBirthday(String birthday, String year) throws ParseException
    {
        int    month = 0,
                day = 0,
                yearBirthday = 0;

        String [] p = birthday.split("\\.");
        day = Integer.parseInt(p[0]);
        month = Integer.parseInt(p[1]);
        yearBirthday = Integer.parseInt(year);

        String date = day + "." + month + "." + yearBirthday;

        DateTimeFormatter df = DateTimeFormat.forPattern("dd.M.yyyy");
        DateTime d1 = df.parseDateTime(date);

        return new SimpleDateFormat("EEEE", Locale.ITALIAN).format(d1.toDate());
    }
}
