package com.javarush.test.level40.lesson08.task01;

import java.io.*;
import java.net.Socket;
import java.net.URL;

/* Отправка GET-запроса через сокет
Перепиши реализацию метода getSite, он должен явно создавать и использовать сокетное соединение Socket с сервером.
Адрес сервера и параметры для GET-запроса получи из параметра url.
Порт используй дефолтный для http.
Классы HttpURLConnection, HttpClient и т.д. не использовать.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        getSite(new URL("http://javarush.ru/social.html"));
    }

    public static void getSite(URL url) throws IOException
    {

        int port = 80;

            try {
                if (url.getPort() != -1) port = url.getPort();

                Socket s = new Socket(url.getHost(), port);
                OutputStream theOutput = s.getOutputStream();
                // no auto-flushing
                PrintWriter pw = new PrintWriter(theOutput, false);
                // native line endings are uncertain so add them manually
                pw.print("GET " + url.getFile() + " HTTP/1.0\r\n");
                pw.print("Accept: text/plain, text/html, text/*\r\n");
                pw.print("\r\n");
                pw.flush();
                InputStream in = s.getInputStream();
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader br = new BufferedReader(isr);
                int c;
                while ((c = br.read()) != -1) {
                    System.out.print((char) c);
                }
            }
            catch (IOException ex) {
                System.err.println(ex);
            }
    }
}