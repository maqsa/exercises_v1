package com.javarush.test.level40.lesson06.task02;

/* Принадлежность точки многоугольнику
Дан многоугольник, заданный координатами своих вершин.
Ребра многоугольника не пересекаются.
Необходимо реализовать метод isPointInPolygon(Point point, List<Point> polygon), который проверит,
принадлежит ли переданная точка многоугольнику.
*/

import java.util.ArrayList;
import java.util.List;

class Point {
    public int x;
    public int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
}


public class Solution {
    public static void main(String[] args) {
        List<Point> polygon = new ArrayList<>();
        polygon.add(new Point(0, 0));
        polygon.add(new Point(0, 10));
        polygon.add(new Point(10, 10));
        polygon.add(new Point(10, 0));

        System.out.println(isPointInPolygon(new Point(5, 5), polygon));
        System.out.println(isPointInPolygon(new Point(100, 100), polygon));
    }

    public static boolean isPointInPolygon(Point point, List<Point> polygon) {
        //напишите тут ваш код
        int i, j, nvert = polygon.size();
        boolean c = false;

        for (i = 0, j = nvert - 1; i < nvert; j = i++) {
            Point polygonPointI = polygon.get(i);
            Point polygonPointJ = polygon.get(j);

            if (((polygonPointI.getY() >= point.getY()) != (polygonPointJ.getY() >= point.getY()))
                    && (point.getX() <= (polygonPointJ.getX() - polygonPointI.getX()) * (point.getY() - polygonPointI.getY()) / (polygonPointJ.getY() - polygonPointI.getY()) + polygonPointI.getX())) {
                c = !c;
            }

        }

        return c;
    }

}
