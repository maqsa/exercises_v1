package com.javarush.test.level40.lesson06.task02;

/**
 * Created by maqsa on 05.02.2016.
 */
public enum RibPosition
{
    TOUCHING, CROSSING, INESSENTIAL
}
