package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/
import java.io.*;
import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        // напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader intreader = new BufferedReader(new FileReader(reader.readLine()));

        ArrayList<Integer> list = new ArrayList<Integer>();

        while (intreader.ready()){

            int data = Integer.parseInt(intreader.readLine());
            if (data % 2 == 0)
                list.add(data);

        }

        Integer[] mas = new Integer[list.size()];
        mas = list.toArray(mas);

        for (int i = 0; i < mas.length; i++){
            for (int j = i+1; j < mas.length; j++){
                if (mas[i] > mas[j]){
                    int a = mas[i];
                    mas[i] = mas[j];
                    mas[j] = a;
                }
            }
        }

        for (int i = 0; i < mas.length; i++){
            System.out.println(mas[i]);
        }

    }
}
